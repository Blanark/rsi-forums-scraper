#!/usr/bin/env python3

import os
import sys
import time
import base64
import common
import hashlib
import binascii
import requests
from getpass import getpass
from selenium import webdriver
from optparse import OptionParser
from urllib.parse import urlencode
from urllib.parse import urlparse, parse_qsl

def fetch_activation_bytes(username, password, AuthCode, options):
    base_url = 'https://robertsspaceindustries.com/'
    
    # Step 0
    opts = webdriver.ChromeOptions()
    opts.add_argument("user-agent=Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko")

    # Step 1
    if '@' in username:  # RSI login using email address
        login_url = "https://robertsspaceindustries.com/connect?jumpto=/spectrum/community/SHDWFORCE"

    player_id = base64.encodestring(hashlib.sha1(b"").digest()).rstrip()  # keep this same to avoid hogging activation slots
    player_id = player_id.decode("ascii")
    print("[*] Player ID is %s" % player_id)

    if options.firefox:
        driver = webdriver.Firefox()
    else:
        if sys.platform == 'win32':
            chromedriver_path = "chromedriver.exe"
        elif os.path.isfile("/usr/lib/chromium-browser/chromedriver"):  # Ubuntu package chromedriver path
            chromedriver_path = "/usr/lib/chromium-browser/chromedriver"
        elif os.path.isfile("/usr/local/bin/chromedriver"):  # macOS + Homebrew
            chromedriver_path = "/usr/local/bin/chromedriver"
        else:
            chromedriver_path = "./chromedriver"

        driver = webdriver.Chrome(chrome_options=opts,
                                  executable_path=chromedriver_path)

    url = login_url
    driver.get(base_url)
    driver.get(url)
    if os.getenv("DEBUG") or options.debug:  # enable if you hit CAPTCHA or 2FA or other "security" screens
        print("[!] Running in DEBUG mode. You will need to login in a semi-automatic way, wait for the login screen to show up ;)")
        time.sleep(32)
    else:
        search_box = driver.find_element_by_id('email')
        search_box.send_keys(username)
        search_box = driver.find_element_by_id('password')
        search_box.send_keys(password)
        search_box.submit()
        time.sleep(4)  # give the page some time to load
        
        print (driver.current_url)   
        if driver.current_url == "https://robertsspaceindustries.com/connect?jumpto=/spectrum/community/SHDWFORCE":
            search_box = driver.find_element_by_id('code')
            search_box.send_keys(AuthCode)
            search_box.submit()
            time.sleep(2)  # give the page some time to load
        else: 
            time.sleep(2)

    # driver.get(url)
    time.sleep(8)
    driver.quit()


if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog [options]", version="%prog 0.2")
    parser.add_option("-d", "--debug",
                      action="store_true",
                      dest="debug",
                      default=False,
                      help="run program in debug mode, enable this for 2FA enabled accounts or for authentication debugging")
    parser.add_option("-f", "--firefox",
                      action="store_true",
                      dest="firefox",
                      default=False,
                      help="use this option to use firefox instead of chrome",)
    parser.add_option("--username",
                      action="store",
                      dest="username",
                      default=False,
                      help="RSI username, use along with the --password and --authcode options")
    parser.add_option("--password",
                      action="store",
                      dest="password",
                      default=False,
                      help="RSI password")
    parser.add_option("--authcode",
                      action="store",
                      dest="AuthCode",
                      default=False,
                      help="2FA Code")                  
    (options, args) = parser.parse_args()

    if options.username and options.password:
        username = options.username
        password = options.password
    else:
        username = input("Username: ")
        password = getpass("Password: ")
        AuthCode = input("2FA Code: ")

    fetch_activation_bytes(username, password, AuthCode, options)
    time.sleep(2)